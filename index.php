<?php include("app/launch.php") ?>
<?php $num = 1; $collec1 = $res->all(); $collec = $res->all(); ?> 
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	<title>Hyperlinks that rock — The resource library of Marianne Plano</title>
</head>
<body>
	<header>
		<h1>Hyperlinks that rock 🔗</h1>
		<nav>
			<?php include("snippets/nav.php") ?>
		</nav>
		<div id="credits">
			This page is based on <a href="https://www.zotero.org" target="_blank">Zotero</a> and the <a href="https://www.zotero.org/support/dev/web_api/v3/" target="_blank">Zotero API</a>.
		</div>
	</header>
	<main>
		<?php include("snippets/collec.php") ?>
	</main>
</body>
</html>
