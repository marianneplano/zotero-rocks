		<?php foreach($collec as $title=>$content): ?>
			<?php $num ++ ?>
			<section class="resources" id="<?= $title ?>">
				<h<?= $num ?>><?= $string->titleize($title) ?></h<?= $num ?>>
				<?php foreach($content as $name=>$value): ?>
					<?php if($name == "resources"): ?>
						<?php include "snippets/resources.php" ?>
					<?php else: ?>
						<?php $collec = array($name=>$value); ?>
						<?php include "snippets/collec.php" ?>
					<?php endif ?>	
				<?php endforeach ?>	
			</section>
			<?php $num -- ?>
		<?php endforeach ?>	