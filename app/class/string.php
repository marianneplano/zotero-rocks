<?php

class Strings{

	public function titleize($str){

		return ucfirst(substr(str_replace("+", " + ", str_replace("-", " ", str_replace("_", " ", $str))), 2));
	}
}