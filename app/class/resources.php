<?php

class Resources extends Pass{

	protected function id(){

		return "5383228";
	}

	protected function wrapper(){

		return "CSTLLYBA";
	}


	public function baseUrl(){

		return "https://api.zotero.org/users/".$this->id()."/";

	}

	public function collecUrl($collec){

		return $this->baseUrl()."collections/".$collec;
	}

	public function getDistant($url){

		$curl_h = curl_init($url);
		curl_setopt($curl_h, CURLOPT_HTTPHEADER, ["Zotero-API-Key: ".$this->key(), "Zotero-API-Version: 3"]);
		curl_setopt($curl_h, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl_h);
		
		return $response;
	}


	public function makeDir($list, $path){

		$name = $list["data"]["name"];
		$dirpath = $path."/".$name;

		if(!file_exists($dirpath)){

			mkdir($dirpath);

		}

		return $dirpath;
	}

	public function writeResources($list, $dirpath, $wrapper, $start){

		$oldfiles = glob($wrapper."/".$list["data"]["name"]."/*.json");
		$resources = $this->getDistant($this->collecUrl($list["data"]["key"])."/items?start=".$start."&limit=100");
		$datas = json_decode($resources, true);
		$newfiles = [];
		$num = 0;
		$start = ($start == 100) ? $start+1 : $start;

		foreach(array_reverse($datas) as $data){

			if($data["data"]["itemType"] !== "attachment"){

				$num ++;
				$neu = $start+$num;
				$newfile = $dirpath."/".sprintf('%03d', $neu)."-".$data["data"]["key"].".json";

				if(!in_array($newfile, $oldfiles)){

					file_put_contents($newfile, json_encode($data["data"]));

				}else{

					$content = file_get_contents($newfile);

					if($content !== $data["data"]){

						file_put_contents($newfile, json_encode($data["data"]));
					}
				}

				$newfiles [] = $newfile;
			}
		}

		$delim = $start+99;

		$this->deleteFiles($newfiles, $oldfiles, $delim);

		$next = json_decode($this->getDistant($this->collecUrl($list["data"]["key"])."/items?start=".($start+100)."&limit=100"), true);

		if(!empty($next)){

			$newstart = $start+100;
			$this->writeResources($list, $dirpath, $wrapper, $newstart);

		}

		return $newfiles;
	}

	public function unlinkRecursive($dir){

		if(is_dir($dir)){

			$files = glob($dir."/*");

			foreach ($files as $file){
				
				if(!is_dir($file)){

					unlink($file);

				}else{

					$this->unlinkRecursive($file);

				}
			}

			rmdir($dir);
			
		}else{

			unlink($dir);
		}
	}


	public function deleteFiles($newfiles, $oldfiles, $limit){

		foreach ($oldfiles as $key=>$oldfile){

			if($key < $limit){

				if(!in_array($oldfile, $newfiles)){

					$this->unlinkRecursive($oldfile);

				}

			}
		}
	}

	public function writeItems($list, $wrapper){

		$dirpath = $this->makeDir($list, $wrapper);
		$this->writeResources($list, $dirpath, $wrapper,0);
	}

	public function getCollecs($id){

		$url = $this->collecUrl($id."/collections");

		return json_decode($this->getDistant($url), true);
	}


	public function writeCollecs($lists, $wrapper){

		$olds = glob($wrapper."/*", GLOB_ONLYDIR);
		$news = [];

		foreach($lists as $list){

			$news [] = $wrapper."/".$list["data"]["name"];

			$this->writeItems($list, $wrapper);

			$subcollecs = $this->getCollecs($list["data"]["key"]);

			if(is_array($subcollecs) && !empty($subcollecs)){

				$subwrap =  $wrapper."/".$list["data"]["name"];
				$this->writeCollecs($subcollecs, $subwrap);
			}
		}


		$this->deleteFiles($news, $olds, 100);
	}

	public function refresh(){

		$lists = $this->getCollecs($this->wrapper());

		$this->writeCollecs($lists, "content");

	}

	public function loadFolder($path, $obj){

		$files = glob($path."/*");
		$obj->resources = [];

		foreach($files as $file){

			if(is_dir($file)){

				$name = pathinfo($file)["filename"];
				$obj->$name = new stdClass;

				$this->loadFolder($file, $obj->$name);

			}elseif(pathinfo($file)["extension"] == "json"){

				$obj->resources[] =  json_decode(file_get_contents($file));
			}

		} 

		if(empty($obj->resources)){

			unset($obj->resources);
		}

		if(is_array($obj->resources)){

			$obj->resources = array_reverse($obj->resources);

		}

		return $obj;
	}

	public function all(){

		$obj = new stdClass;

		return $this->loadFolder("content", $obj);
	}

	public function isFire($tags){

		foreach($tags as $tag) {

			if($tag->tag == "fire"){

				return true;
			}
		}

		return false;
	}

	public function getType($tags){

		if($this->isFire($tags)){

			return "fire";

		}else{

			return "notSoFire";
		}
	}

	public function fireOut($tags){

		foreach($tags as $key=>$tag) {

			if($tag->tag == "fire"){

				unset($tags[$key]);

			}
		}

		return $tags;
	}
}